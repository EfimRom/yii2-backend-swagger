<?php

use yii\db\Schema;
use yii\db\Migration;

class m150703_121624_insert_user extends Migration
{
    public function up()
    {
        $this->insert('{{%user}}',['username'=>'testuser','email'=>'test@mail.com','auth_key'=>'hgfhjgfjksdjk35',
            'status'=>10]);
    }

    public function down()
    {
        echo "m150703_121624_insert_user cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
