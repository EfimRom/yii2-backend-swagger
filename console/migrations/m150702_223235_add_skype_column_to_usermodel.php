<?php

use yii\db\Schema;
use yii\db\Migration;

class m150702_223235_add_skype_column_to_usermodel extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'skype','varchar(255)');
    }

    public function down()
    {
       $this->dropColumn('{{%user}}', 'skype');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
